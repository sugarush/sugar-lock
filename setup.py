__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-lock',
    version='0.0.1',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-lock',
    packages=[
        'sugar_lock'
    ],
    description='A lock class for Redis.',
    install_requires=[
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-asynctest@master',
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-concache@master',
        'aioredis'
    ]
)

About
=====

Sugar Lock provides a `Lock` class backed by **Redis**.

Source
------

The project's `source <https://gitlab.com/sugarush/sugar-lock>`_ is
available on `GitLab`.

Installation
------------

Suagr Lock can be installed with `pip`.

``pip install git+https://gitlab.com/sugarush/sugar-lock@master``

Usage
-----

.. code-block:: python

  import asyncio
  from sugar_concache.redis import RedisDB
  from sugar_lock import Lock

  RedisDB.defaults = {
    'host': 'redis://localhost'
  }

  async def main():
    lock = Lock(key='some-key')
    await lock.acquire()
    # Do something...
    await lock.release()

  asyncio.run(main())

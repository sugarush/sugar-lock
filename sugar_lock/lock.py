from asyncio import sleep, create_task, CancelledError
from uuid import uuid4
from inspect import iscoroutinefunction

import aioredis

from sugar_concache.redis import RedisDB


class Lock(object):
    '''
    A Redis backed lock object.

    :param key: The Redis key to use.
    :param value: The value used to verify lock acquisition.
    '''

    def __init__(self, key=None, value=None):
        if not key:
            raise Exception('Lock: No key provided.')
        self.key = key
        self.value = value
        self.redis = None
        self.sleep = None
        self.watch = None

    async def _connect(self):
        if not self.value:
            self.value = str(uuid4())
        if self.redis:
            return
        else:
            self.redis = await RedisDB.connect(lowlevel=True)

    async def _watch(self):
        if callable(self.key):
            key = self.key()
        else:
            key = self.key
        if not key:
            raise Exception('Lock.acquire: No key provided.')
        conn = await aioredis.create_connection(RedisDB.defaults['host'])
        channel = aioredis.Channel(f'lock:{key}', is_pattern=False)
        await conn.execute_pubsub('SUBSCRIBE', channel)
        try:
            async for message in channel.iter():
                if message.decode() == 'release':
                    self.sleep.cancel()
                break
        except CancelledError:
            conn.close()
            await conn.wait_closed()

    async def acquire(self, expire=5, delay=0.5, locked_callback=None, execute_locked_callback=False):
        '''
        Acquires the lock.

        :param expire: How long before the lock acquisition should expire automatically.
        :param delay: How long between lock acquisition attempts
        '''
        await self._connect()
        if callable(self.key):
            key = self.key()
        else:
            key = self.key
        if not key:
            raise Exception('Lock.acquire: No key provided.')
        await self.redis.execute('SET', f'lock:{key}', self.value, 'NX', 'PX', expire * 1000)
        current = (await self.redis.execute('GET', f'lock:{key}')).decode()
        if current == self.value:
            if locked_callback and execute_locked_callback:
                if iscoroutinefunction(locked_callback):
                    await locked_callback()
                else:
                    locked_callback()
            return
        self.sleep = create_task(sleep(delay))
        if not self.watch or self.watch.done():
            self.watch = create_task(self._watch())
        try:
            await self.sleep # this may be canceled by the watcher
        except CancelledError:
            pass
        await self.acquire(expire, delay, locked_callback, execute_locked_callback=True)

    async def release(self):
        '''
        Releases the lock.
        '''
        await self._connect()
        if callable(self.key):
            key = self.key()
        else:
            key = self.key
        if not key:
            raise Exception('Lock.acquire: No key provided.')
        current = await self.redis.execute('GET', f'lock:{key}')
        if current and current.decode() == self.value:
            await self.redis.execute('DEL', f'lock:{key}')
            await self.redis.execute('PUBLISH', f'lock:{key}', 'release')

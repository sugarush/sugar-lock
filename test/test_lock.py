from asyncio import get_event_loop, gather, sleep

from sugar_asynctest import AsyncTestCase
from sugar_concache.redis import RedisDB

from sugar_lock import Lock


RedisDB.defaults = {
    'host': 'redis://localhost'
}


class LockTest(AsyncTestCase):

    default_loop = True

    async def asyncSetUp(self):
        await RedisDB.set_event_loop(get_event_loop())

    async def test_lock(self):
        state = type('', (), {})()
        state.locked = 0

        alpha = Lock('some-key')
        beta = Lock('some-key')

        async def check_alpha(state):
            await alpha.acquire()
            await sleep(0.01)
            state.locked += 1
            await alpha.release()

        async def check_beta(state):
            await beta.acquire()
            await sleep(0.01)
            state.locked += 1
            await beta.release()

        await gather(check_alpha(state), check_beta(state))

        self.assertEqual(state.locked, 2)

    async def test_lock_callback(self):
        state = type('', (), {})()
        state.locked = 0

        alpha = Lock('some-key')
        beta = Lock('some-key')

        def callback():
            state.locked += 1

        async def check_alpha(state):
            await alpha.acquire(locked_callback=callback)
            await sleep(0.01)
            await alpha.release()

        async def check_beta(state):
            await beta.acquire(locked_callback=callback)
            await sleep(0.01)
            await beta.release()

        await gather(check_alpha(state), check_beta(state))

        self.assertEqual(state.locked, 1)

    async def test_lock_async_callback(self):
        state = type('', (), {})()
        state.locked = 0

        alpha = Lock('some-key')
        beta = Lock('some-key')

        async def callback():
            state.locked += 1

        async def check_alpha(state):
            await alpha.acquire(locked_callback=callback)
            await sleep(0.01)
            await alpha.release()

        async def check_beta(state):
            await beta.acquire(locked_callback=callback)
            await sleep(0.01)
            await beta.release()

        await gather(check_alpha(state), check_beta(state))

        self.assertEqual(state.locked, 1)
